# 致谢

`CopyTranslator`的重生离不开以下人员的贡献

## 界面与交互设计

图标设计师：[Mārtiņš Zemlickis](http://mzemlickis.lv/)

## 代码贡献者
- Mac 维护者 [黎紫珊](https://github.com/Sandural)

## 开源库
所基于的开源库太多，难以罗列，在这里仅列出主要的几项：
- [Electron](https://electronjs.org)：使用 JavaScript, HTML 和 CSS 构建跨平台的桌面应用
- [Vue](http://vuejs.org)：渐进式 JavaScript 框架
- [Element](http://element-cn.eleme.io/#/zh-CN): 一套为开发者、设计师和产品经理准备的基于 Vue 2.0 的桌面端组件库
- [translation.js](https://github.com/Selection-Translator/translation.js): 核心翻译引擎
- [iohook](https://github.com/wilix-team/iohook): 捕获全局鼠标和键盘事件
- [robotjs](http://robotjs.io/): 模拟键盘输入
- [Saladict](https://github.com/crimx/ext-saladict) 沙拉查词-聚合词典划词翻译--Sogou Engine
- [eazydict](https://github.com/keenwon/eazydict) 简单易用的命令行词典
- [dadda-translate-crx](https://github.com/waynecz/dadda-translate-crx) 比较好看的 Chrome 划词翻译(搜狗)插件，自带生词簿及吐司单词记忆，可与有道、扇贝单词同步 
- [iciba-translate-userscript](https://github.com/Firefox-Pro-Coding/iciba-translate-userscript) iciba划词翻译 userscript

## 界面语言维护

- 繁体中文: [Jeff Huang](https://github.com/s8321414)

## 推广

感谢亲爱的朋友们，用户们的推荐和肯定